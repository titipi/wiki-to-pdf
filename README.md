# wiki-to-pdf

🐬.🐬.🐬.

## minimum functioning

* the system is running with the flask web-interface
* the command-line interface should still be checked...
* the flask interface can be reached at http://titipi.org/wiki-to-pdf/
* this list the unfoldable pages, and for each of them generate links to generate html and pdf files

# todo

* write further documentation :P

# credits

These tools are based on the work of Manetta Berends for *[Volumetric Regimes](http://volumetricregimes.xyz)*:

* <https://git.vvvvvvaria.org/mb/volumetric-regimes-book/>
* <https://volumetricregimes.xyz/index.php?title=Unfolded>

... which retook a practice developed by OSP (Sarah Magnan and Gijs de Heij) for *[DiVersions](diversions.constantvzw.org/)*:

* <https://gitlab.constantvzw.org/osp/work.diversions>
* <https://diversions.constantvzw.org/wiki/index.php?title=PublicationUnfolded>

`Mediawiki > Unfolded HTML page > Jinja template > CSS + Paged.js > PDF`
