#!/usr/bin/python3

import threading
import subprocess, shlex
import flask
import urllib, json
import os, git
from update import *
from werkzeug.middleware.proxy_fix import ProxyFix

# To add: server feedback (logging + capacity) using socket io in Flask
# https://towardsdatascience.com/how-to-add-on-screen-logging-to-your-flask-application-and-deploy-it-on-aws-elastic-beanstalk-aa55907730f

# Create the application.
APP = flask.Flask(__name__, static_url_path="/", static_folder="../static")

APP.wsgi_app = ProxyFix(APP.wsgi_app, x_prefix=1)

# ---

PROJECT_NAME = 'wiki-do-pdf'
DIR_PATH = '.' # without trailing slash
PORTNUMBER = 5522
WIKI = 'http://titipi.org/wiki' # remove tail slash '/'
WIKIPDFOUT = 'http://titipi.org/wiki-to-pdf/neu/'

PAGENAME = 'Infrastructural_Interactions'
NAMESPACE = 'Unfolding'
PAGEURL = NAMESPACE + ':' + PAGENAME

CSS_NAMESPACE = 'UnfoldingCSS'
CSSURL = CSS_NAMESPACE + ':' + PAGENAME

STYLESHEET_PAD = 'titipi.css'
STYLESHEET = 'print.css'
TEMP_PATH = '/var/www/html/titipi.org/public_html/wiki-to-pdf'
GIT_PATH = '..'
OUTPUTFOLDER='/var/www/html/titipi.org/public_html/pub'
OUTPUBLIC='http://titipi.org/pub'
NODE='/home/martino/.nvm/versions/node/v14.10.0/bin/node'
PAGEDJSCLI='/home/martino/.nvm/versions/node/v14.10.0/bin/pagedjs-cli'

g = git.cmd.Git(GIT_PATH)


def downloadcss(pagename=PAGENAME, cssfilename=STYLESHEET):
	cssurl = CSS_NAMESPACE + ':' + pagename
	css_unfolded = parse_css(cssurl, pagename, WIKI)
	save_css(css_unfolded, pagename)

def savepdf(pagename=PAGENAME, outputfolder=OUTPUTFOLDER):
    subprocess.run([NODE, PAGEDJSCLI, WIKIPDFOUT+pagename, '-o', outputfolder+'/'+pagename+'.pdf'])

@APP.route('/', methods=['GET'])
def list():
	pagelist = list_pages(WIKI)
	return flask.render_template('./flask/index.html', pagelist=pagelist, namespace=NAMESPACE, cssnamespace=CSS_NAMESPACE, namespacenr=NAMESPACE_NR, wiki=WIKI, title=PROJECT_NAME)


@APP.route('/unfold/<pagename>', methods=['GET'])
def unfold(pagename):
	pageurl=NAMESPACE+ ':' + pagename
	publication_unfolded = update_material_now(pageurl, pagename, WIKI)
	downloadcss(pagename)
	return flask.render_template('./flask/unfolded.html', publication_unfolded=publication_unfolded, wiki=WIKI, namespace=NAMESPACE, cssnamespace=CSS_NAMESPACE, pagename=pagename, pageurl=pageurl, stylesheet=STYLESHEET )


@APP.route('/pagedjs/<pagename>', methods=['GET', 'POST'])
def pagedjs(pagename):
	pageurl=NAMESPACE+ ':' + pagename
	publication_unfolded = update_material_now(pageurl, pagename, WIKI)
	downloadcss() # download the stylesheet pad
	return flask.render_template('./flask/pagedjs.html', publication_unfolded=publication_unfolded, wiki=WIKI, namespace=NAMESPACE, cssnamespace=CSS_NAMESPACE, pagename=pagename, pageurl=pageurl, stylesheet=STYLESHEET)

@APP.route('/neu/<pagename>', methods=['GET', 'POST'])
def neu(pagename):
	pageurl=NAMESPACE+ ':' + pagename
	publication_unfolded = update_material_now(pageurl, pagename, WIKI)
	downloadcss() # download the stylesheet pad
	return flask.render_template('./flask/neu.html', publication_unfolded=publication_unfolded, wiki=WIKI, namespace=NAMESPACE, cssnamespace=CSS_NAMESPACE, pagename=pagename, pageurl=pageurl, stylesheet=STYLESHEET)

@APP.route('/save/<pagename>', methods=['GET', 'POST'])
def save(pagename):
    texists = 0
    for t in threading.enumerate():
        if t.name == 'pdfsaver':
            texists = 1
    if texists:
        return('there is already another pdf saving in process, please be patient...')
    else:
        thread = threading.Thread(name='pdfsaver',target=savepdf, args=(pagename,OUTPUTFOLDER))
        thread.start()    
        return ('saving '+pagename+'.pdf to <a href="'+OUTPUBLIC+'">'+OUTPUBLIC+'</a> ! ( might take some minutes.. ^^ )');

@APP.route('/savehtml/<pagename>', methods=['GET', 'POST'])
def savehtml(pagename):
        pubhtml(pagename, stylesheet=STYLESHEET)
        return ('saving '+pagename+'.html to <a href="'+OUTPUBLIC+'">'+OUTPUBLIC+'</a> !');


@APP.route('/webhook', methods=['POST'])
def respond():
	data = flask.request
	print('git push received')
	if data.headers['X-Gitlab-Token'] == 'unfoldings':
		print('performing git pull')
		repo = data.json['repository']['name'];
		if repo == 'wiki-to-pdf':
			g.pull()
	else:
		print('git push had wrong token!')
	return 'ok'


@APP.route('/css/', methods=['GET'])
def css():
	referrer = flask.request.referrer
	downloadcss()
	if referrer:
		return flask.redirect(referrer)
	else:
		return flask.render_template('./flask/index.html', title=PROJECT_NAME)


@APP.route('/update/', methods=['GET', 'POST'])
def update():
	publication_unfolded = update_material_now(PAGEURL, PAGENAME, WIKI) # download the latest version of the page
	downloadcss()
	# with open(f'{ DIR_PATH }/static/Unfolded.html', 'w') as out:
	# 	out.write(publication_unfolded) # save the html (without <head>) to file

	return flask.render_template('./flask/index.html', title=PROJECT_NAME)


@APP.route('/stylesheet/', methods=['GET'])
def stylesheet():
	return flask.render_template('./flask/stylesheet.html')

if __name__ == '__main__':
	APP.debug=True
	APP.run(port=f'{ PORTNUMBER }')
