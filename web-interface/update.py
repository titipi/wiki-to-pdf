#!/usr/bin/python3

import urllib.request
import os
import re
import io
import json
import jinja2
import lxml.html, lxml.cssselect
import base64
from PIL import Image
import hitherdither

STATIC_FOLDER_PATH = '../static' # without trailing slash
OUT_FOLDER_PATH = '../static/out' # without trailing slash
PUBLIC_STATIC_FOLDER_PATH = '../out' # without trailing slash
OUTPUBFOLDER='/var/www/html/titipi.org/public_html/pub'

WRAPPING_TEMPLATES_DIR = './templates/wrapping-templates/'
TEMPLATES_DIR = './templates/flask/'
NAMESPACE = 'Unfolding'
CSS_NAMESPACE = 'UnfoldingCSS'
NAMESPACE_NR = '3000'


# This uses a low quality copy of all the images
# (using a folder with the name "images-small",
# which stores a copy of all the images generated with:
# $ mogrify -quality 5% -adaptive-resize 25% -remap pattern:gray50 * )
fast = False

def API_request(url, pagename=None, savefile=None):
	"""
		url = API request url (string)
		data =  { 'query':
					'pages' :
						pageid : {
							'links' : {
								'?' : '?'
								'title' : 'pagename'
							}
						}
					}
				}
	"""
	response = urllib.request.urlopen(url).read()
	data = json.loads(response)

	# Save response as JSON to be able to inspect API call
	if pagename:
		if not os.path.exists(f'{ OUT_FOLDER_PATH }/{ pagename }'):
			os.makedirs(f'{ OUT_FOLDER_PATH }/{ pagename }')
	if savefile:
		json_file = f'{ OUT_FOLDER_PATH }/{ pagename }/{ savefile }'
		print('Saving JSON:', json_file)
		with open(json_file, 'w') as out:
			out.write(json.dumps(data, indent=4))
			out.close()

	return data

def download_media(html, images, wiki):
	"""
		html = string (HTML)
		images = list of filenames (str)
	"""
	# check if 'images/' already exists
	if not os.path.exists(f'{ OUT_FOLDER_PATH }/images'):
		os.makedirs(f'{ OUT_FOLDER_PATH }/images')

	# download media files
	for filename in images:
		filename = filename.replace(' ', '_') # safe filenames

		# check if the image is already downloaded
		# if not, then download the file
		if not os.path.isfile(f'{ OUT_FOLDER_PATH }/images/{ filename }'):

			# first we search for the full filename of the image
			url = f'{ wiki }/api.php?action=query&list=allimages&aifrom={ filename }&format=json'
			response = urllib.request.urlopen(url).read()
			data = json.loads(response)

			# we select the first search result
			# (assuming that this is the image we are looking for)
			image = data['query']['allimages'][0]

			# then we download the image
			image_url = image['url']
			image_filename = image['name']
			print('Downloading:', image_filename)
			image_response = urllib.request.urlopen(image_url).read()

			# and we save it as a file
			image_path = f'{ OUT_FOLDER_PATH }/images/{ image_filename }'
			out = open(image_path, 'wb')
			out.write(image_response)
			out.close()

			import time
			time.sleep(3) # do not overload the server

		# replace src link
		image_path = f'{ PUBLIC_STATIC_FOLDER_PATH }/images/{ filename }' # here the images need to link to the / of the domain, for flask :/// confusing! this breaks the whole idea to still be able to make a local copy of the file
		matches = re.findall(rf'src="/wiki/images/.*?px-{ filename }"', html) # for debugging
		if matches:
			html = re.sub(rf'src="/wiki/images/.*?px-{ filename }"', f'src="{ image_path }"', html)
		else:
			matches = re.findall(rf'src="/wiki/images/.*?{ filename }"', html) # for debugging
			html = re.sub(rf'src="/wiki/images/.*?{ filename }"', f'src="{ image_path }"', html)
		# print(f'{filename}: {matches}\n------') # for debugging: each image should have the correct match!

	return html

def imgs_to_base64(html):
	matches = re.findall(rf'src="{PUBLIC_STATIC_FOLDER_PATH}/images/.*"')
	if matches:
		for match in matches:
			imagepath=match.split('src="')[1].split('"')[0].replace(PUBLIC_STATIC_FOLDER_PATH,OUT_FOLDER_PATH)
			base64=dither_image(imagepath)
			html=html.replace(match,'src="'+base64+'"')
	return html

def dither_image(image):
	img=Image.open(image)
	output = io.BytesIO()
	img_dithered = hitherdither.ordered.bayer.bayer_dithering(img, palette, [256/4, 256/4, 256/4], order=8)
	img.save(output, format='PNG')
	hex_data = output.getvalue()
	base64_utf8_str = base64.b64encode(hex_data).decode('utf-8')
	dataurl = f'data:image/png;base64,{base64_utf8_str}'
	return dataurl

def process_refs(html):
	# here we go pick up all the content of <ref> tags from the wiki, and paste it back as pagedjs footnotes!
        html = lxml.html.fromstring(html)

        refs = html.cssselect('sup.reference')
        for ref in refs:
                reflink = ref.cssselect('a')[0].attrib['href']
                target = html.cssselect(reflink)[0]
                referencetext = target.cssselect('.reference-text')[0]
                parent = ref.getparent()
                ref.addnext(referencetext)
                parent.remove(ref)

        wrappers = html.cssselect('.mw-references-wrap')
        for wrap in wrappers:
                parent = wrap.getparent()
                parent.remove(wrap)
        html = lxml.html.tostring(html).decode()
        return html

def clean_up(html):
	"""
		html = string (HTML)
	"""
	html = re.sub(r'\[.*edit.*\]', '', html) # remove the [edit]
	html = re.sub(r'href="/wiki/index.php/', 'href="#', html) # remove the internal wiki links
	html = re.sub(r'&#91;(?=\d)', '', html) # remove left footnote bracket [
	html = re.sub(r'(?<=\d)&#93;', '', html) # remove right footnote bracket ]
	return html

def fast_loader(html):
	"""
		html = string (HTML)
	"""
	if fast == True:
		html = html.replace('/images/', '/images-small/')
		print('--- rendered in FAST mode ---')

	return html

def list_pages(wiki):
	apicall = f'{wiki}/api.php?action=query&list=allpages&format=json&apnamespace='+NAMESPACE_NR
	data = API_request(apicall)
	namespacepages = []
	if 'query' in data:
		if 'allpages' in data['query']:
			for pages in data['query']['allpages']:
				namespacepages.append(pages['title'])

	return namespacepages



def parse_page(pageurl, pagename, wiki):
	"""
		pagename = string
		html = string (HTML)
	"""
	parse = f'{ wiki }/api.php?action=parse&page={ pageurl }&pst=True&format=json'
	data = API_request(parse, pagename, 'data.json')
	# print(json.dumps(data, indent=4))
	if 'parse' in data:
		html = data['parse']['text']['*']
		images = data['parse']['images']
		html = download_media(html, images, wiki)
		html = process_refs(html)
		html = clean_up(html)
		html = fast_loader(html)
	else:
		html = None

	return html


def parse_css(cssurl, pagename, wiki):
	"""
		pagename = string
		html = string (HTML)
	"""
	parse = f'{ wiki }/api.php?action=parse&page={ cssurl }&pst=True&format=json'
	data = API_request(parse, pagename, 'data_css.json')
	# print(json.dumps(data, indent=4))
	if 'parse' in data:
		html = data['parse']['text']['*']
		pre_html=re.compile('<pre.+>(.*?)</pre>', re.DOTALL).search(html)
		css = pre_html.group(1)
	else:
		css = None

	return css

def save(html, pagename, publication_unfolded):
	"""
		html = string (HTML)
		pagename = string
	"""
	if html:

		# save final page that will be used with PagedJS
		template_file = open(f'{ WRAPPING_TEMPLATES_DIR }/template.html').read()
		template = jinja2.Template(template_file)
		html = template.render(publication_unfolded=publication_unfolded, title=pagename)

		html_file = f'{ OUT_FOLDER_PATH }/{ pagename }/index.html'
		print('Saving HTML:', html_file)
		with open(html_file, 'w') as out:
			out.write(html)
			out.close()

		# save extra html page for debugging
		template_file = open(f'{ WRAPPING_TEMPLATES_DIR }/template.inspect.html').read()
		template = jinja2.Template(template_file)
		html = template.render(publication_unfolded=publication_unfolded, title=pagename)

		html_file = f'{ OUT_FOLDER_PATH }/{ pagename }/inspect.html'
		print('Saving HTML:', html_file)
		with open(html_file, 'w') as out:
			out.write(html)
			out.close()

def pubhtml(pagename,stylesheet):
    print('Publishing HTML:', pagename)
    wiki = 'http://titipi.org/wiki' # remove tail slash '/'
    pageurl = NAMESPACE+':'+pagename

    publication_unfolded = update_material_now(pageurl, pagename, wiki) # download the latest version of the page
    publication_base64d = imgs_to_base64(publication_unfolded)

    fullstylesheet = open(f'{OUT_FOLDER_PATH }/{ pagename }/{ stylesheet }').read()
    template_file = open(f'{ TEMPLATES_DIR }/standalone.html').read()
    template = jinja2.Template(template_file)
    html = template.render(publication_unfolded=publication_base64d, pageurl=pageurl,title=pagename,stylesheet=fullstylesheet,pagename=pagename)

    html_file = f'{ OUTPUBFOLDER }/{ pagename }.html'
    print('Saving HTML:', html_file)
    with open(html_file, 'w') as out:
        out.write(html)
        out.close()
    

def save_css(css, pagename, cssfilename='print.css'):

	if css:
		css_file = f'{ OUT_FOLDER_PATH }/{ pagename }/'+cssfilename
		print('Saving CSS:', css_file)
		with open(css_file, 'w') as out:
			out.write(css)
			out.close()


def update_material_now(pageurl, pagename, wiki):
	"""
		pagename = string
		publication_unfolded = string (HTML)
	"""
	publication_unfolded = parse_page(pageurl, pagename, wiki)

	return publication_unfolded

# ---

if __name__ == "__main__":

        wiki = 'http://titipi.org/wiki' # remove tail slash '/'
        pagename = 'Infrastructural_Interactions'
        pageurl = NAMESPACE+':'+pagename
        publication_unfolded = update_material_now(pageurl, pagename, wiki) # download the latest version of the page
        save(publication_unfolded, pagename, publication_unfolded) # save the page to file

        cssurl = CSS_NAMESPACE+':'+pagename
        css_unfolded = parse_css(cssurl, pagename, wiki)
        save_css(css_unfolded, pagename)
